<?php
/**
* Smala Global Web
* @author Smala rabotasmala@gmail.com
* @copyright  2020 Smala Inc
*
*/

// Error Level
error_reporting(-1);

// TIME LOAD PAGE
$load_time_start = microtime(true);

// DEFINE BASE PARAMS
define('IS_SMALA_SECURITY', true);
define('HOMEDIR', dirname(__FILE__) . '/');

// START SITE
include_once HOMEDIR . "core/init.php";
$site = new Init();
$site->start();

// TIME LOAD PAGE SHOW
if (!empty($load_time_start)) {
	$load_time = microtime(true) - $load_time_start;
	echo 'LoadTime: ' . round($load_time, 5) . ' sec';
}