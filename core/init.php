<?php if ( !defined('IS_SMALA_SECURITY') ) die();

// BASE CORE
include_once  HOMEDIR . "core/core.php";
include_once  HOMEDIR . "core/cache.php";
include_once  HOMEDIR . "core/db.php";
include_once  HOMEDIR . "core/config.php";
include_once  HOMEDIR . "core/route.php";
include_once  HOMEDIR . "core/view.php";
include_once  HOMEDIR . "core/helpers/pagination.php";
require_once  HOMEDIR . "core/helpers/templater.php";

include_once  HOMEDIR . "site/controller.php";

class Init {

	public function start() {

		// custom params init
		$this->initialize();
		
		// show html
		echo $this->site();
	}

	public function initialize() {

		// Database init
		DB::in("|=|", "|_|", HOMEDIR . "data");
	}

	private function site() {

		// Route
		Route::in()->set();
		Route::in()->validate();

		if (Config::in()->get('cache')) {
			$cache = Cache::in()->get();
			if ($cache) {
				return $cache;
			}
		}

		// controller start
		$html = Controller::in()->{Route::in()->method}(Route::in()->params);
		

		// set cache
		if (Config::in()->get('cache')) {
			Cache::in()->save($html);
		}

		return $html;
	}
}