<?php if ( !defined('IS_SMALA_SECURITY') ) die();

class Core {
	private static $instances = [];
	public static function in($a = false, $b = false, $c = false, $d = false) {
		$cls = static::class;
		if (!isset(self::$instances[$cls])) {
			self::$instances[$cls] = new static($a, $b, $c, $d);
		}

		return self::$instances[$cls];
	}
}