<?php if ( !defined('IS_SMALA_SECURITY') ) die();

include_once  HOMEDIR . "site/route.php";

class Route extends Core {
	
	public $method;
	public $params;

	public function set() {
		global $site_routes;

		$route = isset($_GET['q']) ? $_GET['q'] : 0;
		$route = trim($route, '/');
		$router_split = explode('/', $route);
	
		// подготовим роутеры с шаблонами
		$templ_string = '{:str}';
		$routes = array();
		foreach($site_routes as $templ => $to) {
			$templ_arr = explode('/', $templ);

			foreach($templ_arr as $tk => $tv) {
				if (strpos($templ, $templ_string) !== false) {
					$templ_arr[$tk] = isset($router_split[$tk]) ? $router_split[$tk] : $templ_string;
				}
			}

			$templ = implode('/', $templ_arr);
			$routes[$templ] = $to;
		}
	
		// если роутер пустой то ставим по умолчанию
		if (empty($route)) {
			$route = 'default';
		}

		// проверка есть ли такой роут полностью
		if (isset($routes[$route])) {
			$this->method = 'page_' . $routes[$route];
		}

		$this->params = $router_split;
		if (empty($this->method))
			die();

	}

	public function validate() {

		//print_r($this);exit();
		if (!method_exists(Controller::in(), $this->method))
			die();
	}
}