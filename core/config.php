<?php if ( !defined('IS_SMALA_SECURITY') ) die();

include_once  HOMEDIR . "site/config.php";

class Config extends Core {
	private $config;

	function __construct(){
		global $site_config;
		$this->config = $site_config;
	}

	public function get($key) {
		if (empty($key) || !isset($this->config[$key]))
			return false;

		return $this->config[$key];
	}
}