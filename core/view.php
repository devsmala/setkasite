<?php if ( !defined('IS_SMALA_SECURITY') ) die();

class View extends Core {
	
	function __construct() {
		ini_set('pcre.backtrack_limit', 1024*1024);
	}

	public function render($template, $data = array()) {

		$template_file = HOMEDIR . 'site/views/'. Config::in()->get('template') . '/' . $template . '.tpl';
		
		if(!file_exists($template_file)) {
			return '';
		}

		$data['template_assets'] = '/assets/' . Config::in()->get('template') . '/';

		return websun_parse_template_path($data, $template_file);
	}
}