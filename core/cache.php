<?php if ( !defined('IS_SMALA_SECURITY') ) die();

class Cache extends Core {
	
	private $cache_key;
	private $cache_file;

	function __construct() {
		$cache_dir = HOMEDIR . 'site/cache';
		if (!is_dir($cache_dir))
			mkdir($cache_dir, 0777);
	}

	public function get(){

		$route_get = json_encode($_GET);
		$route_post = json_encode($_POST);

		$this->cache_key = md5($route_get . $route_post);
		$this->cache_file = HOMEDIR . 'site/cache/' . $this->cache_key;

		if (!file_exists($this->cache_file)) {
			return false;
		}

		if ($this->check_time()) {
			return false;
		}

		return file_get_contents($this->cache_file);
	}

	private function check_time() {

		// $this->remove_olds();
		// filemtime(HOMEDIR . 'site/cache/' . );
		// $cache_files = glob();
		

		$time = time() - filemtime($this->cache_file);
		if ($time > Config::in()->get('cache_time')) {
			unlink($this->cache_file);
			return true;
		}

		return false;
	}

	public function save($html){
		file_put_contents($this->cache_file, $html);
	}
}