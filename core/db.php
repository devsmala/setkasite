<?php if ( !defined('IS_SMALA_SECURITY') ) die();

class DB extends Core {
	private $STR_ROW;
	private $STR_COLUMN;
	private $DATA_FOLDER;

	public $where = array();
	public $where_in = array();
	public $limit = false;
	public $orderby = array();
	public $offset = 0;
	private $results = array();
	private $titles = false;

	private $index = array(

		// индекс id значения (check)
		'index_key' => array(
			'texts' => array('text'),
			'links' => array('link')
		),

		/*// индекс значение (сохранить связи параметра  таблице)
		'index_val' => array(
		),

		// доступ к записям по ключам
		'index_val_nav' => array(
		)*/
	);
	
	function __construct($STR_COLUMN, $STR_ROW, $DATA_FOLDER){
		$this->STR_ROW = $STR_ROW;
		$this->STR_COLUMN = $STR_COLUMN;
		$this->DATA_FOLDER = $DATA_FOLDER;
	}

	public function where($key, $val = false, $op = '=') {
		if(empty($key)) {
			return false;
		}

		if(is_array($key) && $val === false) {
			$this->where[] = $key;
		} else {
			$this->where[$key] = $val;
		}

		return $this;
	}


	public function where_in($key, $vals, $op = '') {
		if(empty($key)) {
			return false;
		}

		$this->where_in[$key] = $vals;

		return $this;
	}  

	/*public function count($tbl_name) {
		if(empty($tbl_name)) {
			return false;
		}

		$tbl_patch = $this->check_table($tbl_name);
		if(empty($tbl_patch)) {
			return false;
		}

		$file_index = $tbl_patch.'/index';
		if(!file_exists($file_index)) {
			return 0;
		}

		$indexData = file_get_contents($file_index);
		if(empty($indexData)) {
			return 0;
		}

		$indexData = explode($this->STR_ROW, $indexData);
		if(count($indexData) == 1 && empty($indexData[0])) {
			return 0;
		}

		return count(explode($this->STR_ROW, $indexData));
	}*/

	public function count($tbl_name = false) {
		if(!empty($this->results)) {
			$this->endquery();

			$results = $this->results;
			$this->results = array();
			return count($results);
		}

		if(empty($tbl_name)) {
			$this->endquery();	
			return false;
		}

		return count($this->get($tbl_name)->rows());
	}

	public function count_all($tbl_name = false) {
		
		$tbl_patch = $this->check_table($tbl_name);
		if(empty($tbl_patch)) {
			return 0;
		}

		$content = file_get_contents($tbl_patch . '/data');
		return (int)substr_count($content, $this->STR_ROW);
	}
	

	private function to_keys($results, $keys, $index_return = false) {
		if(empty($results)) {
			return false;
		}

		if(is_array($keys)) {
			$tokeys = $keys;
		} else {
			$tokeys = array($keys);
		}

		$newResults = array();
		foreach($results as $r) {
			foreach($tokeys as $tk) {
				$newResults[] = $r[$tk];
			}
		}

		if($index_return !== false) {
			return  $newResults[$index_return];
		}

		return  $newResults;
	}

	public function rows($keys = false) {

		if(empty($this->results)) {
			$this->endquery();
			return false;
		}

		if($keys !== false) {
			$results = $this->toLimits($this->results);
			return $this->to_keys($results, $keys);
		}

		$results = $this->toLimits($this->results);

		return $results;
	}

	public function limit($limit) {
		$this->limit = $limit;
		return $this;
	}

	public function offset($offset) {
		$this->offset = $offset;
		return $this;
	}

	public function orderby($sort, $type = 'DESC') {
		$this->orderby = $offset;
		return $this;
	}

	public function toLimits($results) {
		if($this->limit === false && $this->offset == 0) {
			$this->endquery();
			return $results;
		}

		$offset = 0;
		if($this->offset != 0) {
			$offset = $this->offset; 
		}

		$limit = false;
		if(!empty($this->limit)) {
			$limit = $this->limit;
		}

		if($limit !== false) {
			$results = array_slice($results, $offset, $limit);
		} else {
			$results = array_slice($results, $offset);
		}

		$this->endquery();

		return $results;
	}

	public function row($keys = false) {
		if(empty($this->results)) {
			$this->endquery();
			return false;
		}

		$results = $this->results;
		if ($this->orderby) {
			//$results = $this->toOrder($results);
		}

		$this->endquery();
		if($keys !== false) {
			return $this->to_keys($results, $keys, 0);
		}
	
		return $results[0];

	}

	public function endquery() {
		$this->where_in = array();
		$this->where = array();
		$this->limit = false;
		$this->offset = 0;
		$this->results = array();
	}

	private function save_file_data($file_data, $results) {
		$data_contents = array(); 

		if(!empty($results)) {
			foreach($results as $res) {
				$data_contents[] = $this->insert_prep($file_data, $res);
			}
		}

		if(!empty($data_contents)) {
			$data_contents = implode($this->STR_ROW, $data_contents);
		} else {
			$data_contents = "";
		}

		// error_log("save_file_data ".microtime()." - ".$file_data." \n", 3, "./__logger_time.txt");

		$fp = fopen($file_data, 'w');
		fwrite($fp, $data_contents);
		fclose($fp);
	}

	private function get_file_data($file) {
		if (!file_exists($file)) {
			$this->endquery();
			return false;
		}


		$file_data = file_get_contents($file);
		if(empty($file_data)) {
			return false;
		}

		$data_contents = explode($this->STR_ROW, $file_data);
		if(count($data_contents) == 1 && empty($data_contents[0])) {
			$this->endquery();
			return false;
		}

		$result = array();
		foreach($data_contents as $d) {
			if (empty($d)) continue;
			$result[] = explode($this->STR_COLUMN, $d);
		}
				
		if(empty($result)) {
			$this->endquery();
			return false;
		}

		$path = trim($file, 'data');
		$path = trim($path, '/');
		$keys = $this->get_keys($path);
		foreach($result as $rk => $rv) {
			$tmp = array();
			foreach ($rv as $vk => $vv) {
				$tmp[$keys[$vk]] = $vv;				
			}

			$result[$rk] = $tmp;
		}

		return $result;


		// $path = trim($file, 'data');
		// $path = trim($path, '/');





		/*$fp = fopen($file, 'r');
		$file_data = '';
		while($str = fgets($fp, 1024)) {
			$file_data .= $str;
		}

		if(empty($file_data)) {
			$this->endquery();
			return false;
		}

		$rows = explode($this->STR_ROW, $file_data);
		if (empty($rows[0])) {
			unset($rows[0]);
		}

		if(empty($rows)) {
			$this->endquery();
			return false;
		}

		$keys = $this->get_keys($path);
		$results = array();
		foreach($rows as $r) {
			if (empty($r)) {
				continue;
			}
			
			$r = explode($this->STR_COLUMN, $r);
			if (empty($r)) {
				continue;
			}

			$result = array();
			foreach($r as $ind => $re) {
				$result[$keys[$ind]] = $re;
			}

			if (empty($result)) {
				continue;
			}

			$results[] = $result;
		}

		if (empty($results)) {
			$this->endquery();
			return false;
		}

		return $results;*/
	}


	private function get_where($row, $tbl_name = false, $debug = false) {
		if(empty($this->where) && empty($this->where_in)) {
			return true;
		}
			
		if(!empty($this->where)) {
			foreach($this->where as $key => $val) {
				if(!isset($row[$key])) {
					return false;
				}

				if($row[$key] != $val) {
					return false;
				}
			}
		}

		if(!empty($this->where_in)) {
			foreach($this->where_in as $wikey => $wivals) {

				if(!isset($row[$wikey])) {
					return false;
				}
				if(!in_array($row[$wikey], $wivals)) {
					return false;
				} 
				
			}
		}

		return true;
	}

	/*public function check($tbl_name) {
		$index_key = $this->index['index_key'][$tbl_name];
		foreach($index_key as $k) {
			$file_data = $tbl_patch.'/data';
		}
	}*/

	public function get($tbl_name){
		$tbl_patch = $this->check_table($tbl_name);
		if(empty($tbl_patch)) {
			return false;
		}

		$file_data = $tbl_patch.'/data';
		$this->results = array();

		if(!file_exists($file_data)) {
			return $this;
		}

		if(empty($this->where) && empty($this->where_in)) {
			$this->results = $this->get_file_data($file_data);
			return $this;
		}

		$results = $this->get_file_data($file_data);
		if (empty($results)) {
			return $this;
		}

		// $this->count_table = to work
		foreach($results as $k => $res) {
			if($this->get_where($res, $tbl_name) !== false){
				$this->results[] = $res;
			}
		}

		return $this;
	}


	public function check_table($tbl_name, $init = true) {

		if ($init) {
			if(!is_dir($this->DATA_FOLDER)) {
				mkdir($this->DATA_FOLDER, 0777);
			}

			if(!file_exists($this->DATA_FOLDER . '/index.html')) {
				touch($this->DATA_FOLDER . '/index.html');
			}

			if(!is_dir($this->DATA_FOLDER.'/'.$tbl_name)) {
				mkdir($this->DATA_FOLDER.'/'.$tbl_name, 0777);
			}

			if(!file_exists($this->DATA_FOLDER . '/' .$tbl_name. '/index.html')) {
				touch($this->DATA_FOLDER . '/' .$tbl_name. '/index.html');
			}
		}

		return $this->DATA_FOLDER.'/'.$tbl_name;
	}

	private $titles_keys = [];
	private function save_keys($path, $data){

		$keys = $this->get_keys($path);

		$newfield=0;
		$new_keys = array_keys($data);
		foreach($new_keys as $nk) {
			if (!in_array($nk, $keys)) {
				$newfield = 1;
				$keys[] = $nk;
			}
		}

		if ($newfield) {
			$this->titles_keys[$path] = $keys;
			$strkeys = implode($this->STR_COLUMN, $keys);
			file_put_contents($path . '/keys', $strkeys);

			unset($strkeys);
		}

		// set data
		$newdata = array();
		foreach($keys as $ik => $k) {
			$newdata[$ik] = $data[$k];
		}

		return $newdata;
	}

	private function get_keys($path){
		if (isset($this->titles_keys[$path])) {
			$keys = $this->titles_keys[$path];
		} elseif (file_exists($path . '/keys')) {
			$keys = file_get_contents($path . '/keys');
			$keys = explode($this->STR_COLUMN, $keys);
		} else {
			$keys = array();
		}

		return $keys;
	}

	public function insert_prep($tbl_patch, $data) {
		if(is_array($data)) {
			$data = $this->save_keys($tbl_patch, $data);
			$data = implode($this->STR_COLUMN, $data);
		}

		return $data;
	}

	public function get_prep($data) {

		$res = unserialize(htmlspecialchars_decode($data));

		//$decode = htmlspecialchars_decode($data);

		/*if (strpos($data, '20170621') !== false ) {
			//print_r(unserialize($data) );
			var_dump($data);exit();
		}*/

		return $res;
	}

	public function insert_get_key($data) {
		return md5($data);
	}

	public function update($tbl_name, $data, $debug = false) {
		if(empty($tbl_name) || empty($data)) {
			return false;
		}

		$tbl_patch = $this->check_table($tbl_name);
		if(empty($tbl_patch)) {
			return false;
		}

		$file_data = $tbl_patch.'/data';
		$results = $this->get_file_data($file_data);
		if (empty($results)) {
			$this->endquery();
			return false;
		}

		foreach($results as $k => $res) {
			if($this->get_where($res) !== false){
				foreach($data as $key => $val) {
					$results[$k][$key] = $val;
				}
			}
		}

		// save contents
		$data_contents = array(); 
		if(!empty($results)) {
			foreach($results as $res) {
				$data_contents[] = $this->insert_prep($tbl_patch, $res);
			}
		}

		if(!empty($data_contents)) {
			$data_contents = implode($this->STR_ROW, $data_contents);
		} else {
			$data_contents = "";
		}

		if (!copy($file_data, $file_data . '_tmp')) {
			die();
		}

		file_put_contents($file_data . '_tmp', $data_contents);
		// @end save contents

		rename($file_data . '_tmp', $file_data);

		$this->endquery();

		return true;
	}

	public function delete($tbl_name, $debug = false) {
		if(empty($tbl_name)) {
			return false;
		}

		$tbl_patch = $this->check_table($tbl_name);
		if(empty($tbl_patch)) {
			return false;
		}

		$file_data = $tbl_patch.'/data';
		$results = $this->get_file_data($file_data);
		if (empty($results)) {
			$this->endquery();
			return false;
		}

		foreach($results as $k => $res) {
			if($this->get_where($res) !== false){
				unset($results[$k]);
			}
		}

		// save contents
		$data_contents = array(); 
		if(!empty($results)) {
			foreach($results as $res) {
				$data_contents[] = $this->insert_prep($res);
			}
		}

		if(!empty($data_contents)) {
			$data_contents = implode($this->STR_ROW, $data_contents);
		} else {
			$data_contents = "";
		}

		file_put_contents($file_data, $data_contents);

		$this->endquery();

		return true;
	}


	public function truncate($tbl_name) {
		if(empty($tbl_name)) {
			return false;
		}

		$tbl_patch = $this->check_table($tbl_name, false);
		if(empty($tbl_patch)) {
			return false;
		}


		@unlink($tbl_patch.'/data');
		@unlink($tbl_patch.'/keys');
		@unlink($tbl_patch.'/index.html');
		@rmdir($tbl_patch);

		return true;
	}

	public function insert($tbl_name, $data, $count = 0) {
		if(empty($tbl_name) || empty($data)) {
			return false;
		}

		$tbl_patch = $this->check_table($tbl_name);
		if(empty($tbl_patch)) {
			return false;
		}

		$insert = $this->insert_prep($tbl_patch, $data);
		if (file_exists($tbl_patch.'/data')) {
			if (!copy($tbl_patch.'/data', $tbl_patch.'/data_tmp')) {
				die();
			}

			$fp = fopen($tbl_patch.'/data_tmp', 'a');
			fwrite($fp, $this->STR_ROW . $insert);
			fclose($fp);
			rename($tbl_patch.'/data_tmp', $tbl_patch.'/data');	
		} else {
			$fp = fopen($tbl_patch.'/data', 'a');
			fwrite($fp, $this->STR_ROW . $insert);
			fclose($fp);
		}

		unset($insert);
		$this->endquery();
		return true;
	}

	private $opens = array();
	private function f_open($file) {
		$open = fopen($file, 'a');
		$this->opens[] = $open;
		return $open;
	}

	private function f_write($fp, $data) {
		$save = fwrite($fp, $data.$this->STR_ROW);
		
		if (empty($save)) {
			//error_log(" \n".(int)$data."     \n  ===== Данные не записались\n", 3, "./__logger_FileDb.txt");
		}

		return $save;
	}

	private function f_close() {
		if(empty($this->opens)) {
			return false;
		}

		foreach($this->opens as $open) {
			fclose($open);
		}

		$this->opens = array();

		return true;
	}


	public function insert_batch($tbl_name, $data) {
		if(empty($tbl_name) || empty($data)) {
			return false;
		}

		$tbl_patch = $this->check_table($tbl_name);
		if(empty($tbl_patch)) {
			return false;
		}

		if (file_exists($tbl_patch.'/data')) {
			if (!copy($tbl_patch.'/data', $tbl_patch.'/data_tmp')) {
				die();
			}

			$fp = fopen($tbl_patch.'/data_tmp', 'a');
		} else {
			$fp = fopen($tbl_patch.'/data', 'a');
		}
		
		foreach($data as $d) {
			unset($insert);
			$insert = $this->insert_prep($tbl_patch, $d);
			fwrite($fp, $this->STR_ROW.$insert);
		}

		fclose($fp);

		if (file_exists($tbl_patch.'/data_tmp')) {
			rename($tbl_patch.'/data_tmp', $tbl_patch.'/data');	
		}

		$this->endquery();
		return true;
	}
}