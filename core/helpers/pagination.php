<?php if ( !defined('IS_SMALA_SECURITY') ) die();

class Pagination extends Core {

	public $param = 'p';
	public $page_views = 6;

	public function get($table, $limit = 10, $ishtml = false, $url = '', $objects = false) {

		if (empty($objects)) {
			$total = DB::in()->count_all($table);
		} else {
			$total = count($objects);
		}

		$page = (int)isset($_GET[$this->param]) ? $_GET[$this->param] : 0;
		$page = ($page < 1) ? 1 : $page;
		$offset = ($page == 1) ? 0 : ($limit * ($page-1));

		$pagination = array('page' => $page, 'offset' => $offset, 'limit' => $limit, 'total' => $total);
		if ($ishtml) {
			$pagination['html'] = $this->render($ishtml, $pagination, $url);
		}

		if (!empty($objects)) {
			$pagination[$table] = array_slice($objects, $offset, $limit);
		}

		return $pagination;
	}

	private function render($template, $data, $url = '') {
		
		$add = '?';
		$param = $url . $add . $this->param . '=';

		$page_max = ceil($data['total'] / $data['limit']);
		$page_min = 1;

		$prev = ($data['page'] > $page_min) ? ($data['page']-1) : false;
		$next = ( ($data['page']+1) < $page_max) ? ($data['page']+1) : false;

		$pages_tmp = 0;
		$page_views = array();
		$no_write = 0;

		$pages_tmp_min = 1;
		foreach (range($page_min, $page_max) as $page) {
			$pages_tmp++;
			

			if ($pages_tmp == $this->page_views ) {
				if ($no_write) {
					
					$page_views[$pages_tmp] = $page;

					break;
				} else {
					$pages_tmp = 1; 
				}
			}

			$page_views[$pages_tmp] = $page;
			

			
			if ($data['page'] == $page) {
				$no_write = 1;
			}
		}

		sort($page_views);

		if ((int)$data['page'] == (int)$page_views[0] && (int)$data['page'] >= (int)$this->page_views) {

			array_unshift($page_views, ($data['page']-1) );

			$mkey = array_keys($page_views);
			$mkey = max($mkey);

			unset($page_views[$mkey]);

		} else {

			$mkey = array_keys($page_views);
			$mval_max = max($mkey);
			$mval_min = min($mkey);

			$mval_max = $page_views[$mval_max];
			if ($mval_max == $page_max) {
				$mval_min = $page_views[$mval_min];
				array_unshift($page_views, ($mval_min-1) );
			}
		
		}
		
		$data['prev'] = ($prev != false) ? $param . $prev : false;
		$data['next'] = ($next != false) ? $param . $next : false;

		$data['prev'] = ($prev == 1) ? $url : $data['prev'];

		$data['page_views'] = array();
		foreach($page_views as $pv) {
			if (!$pv)
				continue;

			$class = ($pv == $data['page']) ? 'active' : '';
			$urll = ($pv == $data['page']) ? 'javascript:void(0);' : $param . $pv;
			$urll = ($pv == 1) ? $url : $urll;

			$data['page_views'][] = array(
				'url' => $urll, 
				'class' => $class,
				'page' => $pv,
			);
		}

		return View::in()->render($template, $data);
	}
}