<footer id="gtco-footer" role="contentinfo">
	<div class="container">
		<div class="row row-pb-md">
			<div class="col-md-12">
				<h3 class="footer-heading">Most Popular</h3>
			</div>
			<div class="col-md-4">
				<div class="post-entry">
					<div class="post-img">
						<a href="#"><img src="{*template_assets*}images/img_1.jpg" class="img-responsive" alt="Free HTML5 Bootstrap Template by GetTemplates.co"></a>
					</div>
					<div class="post-copy">
						<h4><a href="#">How Web Hosting Can Impact Page Load Speed</a></h4>
						<a href="#" class="post-meta"><span class="date-posted">4 days ago</span></a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="post-entry">
					<div class="post-img">
						<a href="#"><img src="{*template_assets*}images/img_2.jpg" class="img-responsive" alt="Free HTML5 Bootstrap Template by GetTemplates.co"></a>
					</div>
					<div class="post-copy">
						<h4><a href="#">How Web Hosting Can Impact Page Load Speed</a></h4>
						<a href="#" class="post-meta"><span class="date-posted">4 days ago</span></a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="post-entry">
					<div class="post-img">
						<a href="#"><img src="{*template_assets*}images/img_3.jpg" class="img-responsive" alt="Free HTML5 Bootstrap Template by GetTemplates.co"></a>
					</div>
					<div class="post-copy">
						<h4><a href="#">How Web Hosting Can Impact Page Load Speed</a></h4>
						<a href="#" class="post-meta"><span class="date-posted">4 days ago</span></a>
					</div>
				</div>
			</div>
		</div>

		<div class="row copyright">
			<div class="col-md-12 text-center">
				<p>
					<small class="block">&copy; 2016 Free HTML5. All Rights Reserved.</small> 
					<small class="block">Designed by <a href="http://gettemplates.co/" target="_blank">GetTemplates.co</a> Demo Images: <a href="http://unsplash.com/" target="_blank">Unsplash</a></small>
				</p>
				<p>
					<ul class="gtco-social-icons">
						<li><a href="#"><i class="icon-twitter"></i></a></li>
						<li><a href="#"><i class="icon-facebook"></i></a></li>
						<li><a href="#"><i class="icon-linkedin"></i></a></li>
						<li><a href="#"><i class="icon-dribbble"></i></a></li>
					</ul>
				</p>
			</div>
		</div>

	</div>
</footer>