<nav aria-label="Page navigation">
	<ul class="pagination">
		{?*prev*}
			<li>
				<a href="{*prev*}" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
				</a>
			</li>
		{?}

		{%*page_views*}
			<li class="{*page_views:class*}">
				<a href="{*page_views:url*}">{*page_views:page*}</a>
			</li>
		{%}

		{?*next*}
			<li>
				<a href="{*next*}" aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
				</a>
			</li>
		{?}
	</ul>
</nav>