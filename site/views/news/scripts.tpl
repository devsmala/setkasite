
	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="{*template_assets*}js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="{*template_assets*}js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="{*template_assets*}js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="{*template_assets*}js/jquery.waypoints.min.js"></script>
	<!-- Stellar -->
	<script src="{*template_assets*}js/jquery.stellar.min.js"></script>
	<!-- Main -->
	<script src="{*template_assets*}js/main.js"></script>

	</body>
</html>
