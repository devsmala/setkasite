{* +head.tpl *}
<div class="gtco-loader"></div>
<div id="page">
	{* +navigation.tpl *}
	{* +header.tpl *}
	<div id="gtco-main">
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-12">
					<ul id="gtco-post-list">
						{%*news*}
						<li class="one-half entry animate-box" data-animate-effect="fadeIn">
							<a href="/{*news:cat_alias*}/{*news:alias*}">
								<div class="entry-img" style="background-image: url({*news:image*})"></div>
								<div class="entry-desc">
									<h3>{*news:title*}</h3>
									<p>{*news:text*}</p>
								</div>
							</a>
							<a href="/{*news:cat_alias*}/" class="post-meta">
								{*news:category*} 
								<span class="date-posted">{*news:date*}</span>
							</a>
						</li>
						{%}
					</ul>	
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					{*pagination_news*}
				</div>
			</div>
		</div>
	</div>
	{* +footer.tpl *}
</div>
{* +scripts.tpl *}
