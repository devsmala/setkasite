<?php if ( !defined('IS_SMALA_SECURITY') ) die();

class Controller extends Core {

	public function page_home() {

		// news list pagination
		$pagination = Pagination::in()->get('news', 10, 'pagination_news', '/');
		$data['pagination_news'] = $pagination['html'];
		
		// news list
		$data['news'] = DB::in()->get('news')
			->offset($pagination['offset'])
			->limit($pagination['limit'])->rows();

		// news top big
		// $data['item_top'] = DB::in()->get('news')->orderby('date', 'desc')->limit(1)->row();

		return View::in()->render('home', $data);
	}

	public function page_category($params) {

		$cat_alias = $params[0];
		if (empty($cat_alias)) {
			$this->page_404();
		}


		// news list pagination
		$newsGet = DB::in()->where('cat_alias', $cat_alias)->get('news')->rows();
		$pagination = Pagination::in()->get('news', 10, 'pagination_news', '/', $newsGet);
		$data['pagination_news'] = $pagination['html'];
		
		// news list
		$data['news'] = $pagination['news'];


		if (empty($data['news'])) {
			$this->page_404();
		}

		return View::in()->render('category', $data);


	}

	public function page_single($params) {

		$alias = $params[1];
		if (empty($alias)) {
			$this->page_404();
		}

		// single post
		$data['post'] = DB::in()->where('alias', $alias)->get('news')->row();
		if (empty($data['post'])) {
			$this->page_404();
		}

		return View::in()->render('item', $data);
	}

	public function page_404() {
		$data = array();
		die('404');
		// return $this->core->view->render('error_404', $data);
	}


}