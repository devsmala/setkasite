<?php if ( !defined('IS_SMALA_SECURITY') ) die();

$site_routes['default'] = 'home';

$site_routes['{:str}'] = 'category';
$site_routes['{:str}/{:str}'] = 'single';

$site_routes['o-nas'] = 'about';

// $site_routes['/item/'] = 'home';
