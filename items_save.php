<?php

error_reporting(-1);

define('IS_SMALA_SECURITY', true);
define('HOMEDIR', dirname(__FILE__) . '/');

include_once(HOMEDIR . 'core/database.php');
$DB = new Database("|=|", "|_|", HOMEDIR . "data");


$DB->truncate('news');

function gen_alias($st) {
	$st = strtolower($st);

	$st = strtr($st, 
		"абвгдежзийклмнопрстуфыэАБВГДЕЖЗИЙКЛМНОПРСТУФЫЭ",
		"abvgdegziyklmnoprstufieABVGDEGZIYKLMNOPRSTUFIE"
	);
	$st = strtr($st, array(
		'ё'=>"yo",    'х'=>"h",  'ц'=>"ts",  'ч'=>"ch", 'ш'=>"sh",  
		'щ'=>"shch",  'ъ'=>'',   'ь'=>'',    'ю'=>"yu", 'я'=>"ya",
		'Ё'=>"Yo",    'Х'=>"H",  'Ц'=>"Ts",  'Ч'=>"Ch", 'Ш'=>"Sh",
		'Щ'=>"Shch",  'Ъ'=>'',   'Ь'=>'',    'Ю'=>"Yu", 'Я'=>"Ya",
		' ' => "-",   ',' => "",
	));

	return $st;
}

$items = array();
$time = time();

$images = array(
	'/assets/news/images/img_1.jpg', 
	'/assets/news/images/img_2.jpg', 
	'/assets/news/images/img_3.jpg',
);

$uid = 1;


while (true) {
	$time = $time - 86000;

	$date = date("Y-m-d", $time);
	$uid++;

	$item = array(
		'id' => $uid,
		'alias' => '',
		'title' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit ' . $uid,
		'text' => 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
		'date' => $date,
		'image' => $images[rand(0,2)],
		'category' => 'Business Corp',
		'cat_alias' => '',
	);

	$item['alias'] = gen_alias($item['title']);
	$item['cat_alias'] = gen_alias($item['category']);

	$items[] = $item;

	if (count($items) >= 50) {
		break;
	}
}


$DB->insert_batch('news', $items);

exit('ok');
// print_r($items);exit();
